﻿using System;
using System.Collections.Generic;

namespace Shared
{
    public class Models
    {
        public class Seat
        {
            public int Row { get; set; }
            public int Column { get; set; }
            public bool IsTaken { get; set; }
        }

        public class CinemaShow
        {
            public Seat[,] Seats { get; set; }
            public int Rows { get; set; }
            public int Columns { get; set; }
            public int MinDistance { get; set; }
            public CinemaShow(int rows, int columns, int min_distance)
            {
                Rows = rows;
                Columns = columns;
                MinDistance = min_distance;
                Seats = new Seat[Rows, Columns];
                for (int i = 0; i < Rows; i++)
                {
                    for (int j = 0; j < Columns; j++)
                    {
                        Seats[i, j] = new Seat() {
                            Row = i,
                            Column = j,
                            IsTaken = false
                        };
                    }
                }
            }
        }

        public class AvailableSeatRow
        {
            public int Row { get; set; }
            public List<int> Columns { get; set; }
        }
    }
}
