﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.Utils
{
    public class CinemaSettings
    {
        public int Rows { get; set; }
        public int Columns { get; set; }
        public int MinDistance { get; set; }
    }
}
