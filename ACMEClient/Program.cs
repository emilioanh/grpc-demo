﻿using System;
using Grpc.Net.Client;
using ACMEBooking;
using System.Threading.Tasks;

namespace ACMEClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            using var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new Booking.BookingClient(channel);
            var reply = client.CheckOpenSeatsForPurchase(
                new OpenSeatsRequest { SeatsToBook = 3 });
            Console.WriteLine("Message:\n" + reply.Message);
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}
