﻿using ACMEBooking;
using ACMEBooking.Repository;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Shared.Models;

namespace ACMEBooking
{
    public class BookingService : Booking.BookingBase 
    { 
        private readonly ILogger<BookingService> _logger;
        private readonly IRepository _cinemaRepository;
        public BookingService(ILogger<BookingService> logger, IRepository cinemaRepository)
        {
            _logger = logger;
            _cinemaRepository = cinemaRepository;
        }

        public override Task<OpenSeatsReply> CheckOpenSeatsForPurchase(OpenSeatsRequest request, ServerCallContext context)
        {
            string reply = "";
            var openSeats = _cinemaRepository.PopulateOpenSeats(request.SeatsToBook);
            foreach (var item in openSeats)
            {
                reply += String.Format("Row {0} Set {1} Seats", item.Row, item.Columns.Count);
                foreach (var columnNum in item.Columns)
                {
                    reply += String.Format(" ({0}, {1})", item.Row, columnNum.ToString());
                }
                reply += "\n";
            }

            return Task.FromResult(new OpenSeatsReply
            {
                Message = reply
            });
        }

        public override Task<ReserveSeatReply> ReserveSeats(ReserveSeatRequest request, ServerCallContext context)
        {
            List<Seat> reservedSeats = new List<Seat>();
            foreach (var seat in request.Seats)
            {
                reservedSeats.Add(new Seat() {
                    Row = seat.Row,
                    Column = seat.Column,
                    IsTaken = false
                });
            }

            string reply = _cinemaRepository.ReserveSeats(reservedSeats);

            return Task.FromResult(new ReserveSeatReply
            {
                Message = reply
            });
        }
    }
}
