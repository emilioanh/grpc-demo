﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Shared.Models;

namespace ACMEBooking.Repository
{
    public interface IRepository
    {
        string ReserveSeats(List<Seat> reservedSeats);

        List<AvailableSeatRow> PopulateOpenSeats(int seatCount);
    }
}
