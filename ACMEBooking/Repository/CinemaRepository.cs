﻿using Shared.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using static Shared.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace ACMEBooking.Repository
{
    public class CinemaRepository : IRepository
    {
        private CinemaShow _cinemaShow;
        private readonly CinemaSettings _cinemaSettings;

        public CinemaRepository(IOptions<CinemaSettings> cinemaSettings)
        {
            _cinemaSettings = cinemaSettings.Value;
            if (_cinemaSettings != null)
            {
                _cinemaShow = new CinemaShow(_cinemaSettings.Rows, _cinemaSettings.Columns, _cinemaSettings.MinDistance);
            }
        }

        /// <summary>
        /// Surround the seats with automatic occupied spaces so as not to let user book the spot
        /// </summary>
        /// <param name="reservedSeats"></param>
        /// <returns>SUCCEED MESSAGE</returns>
        public string ReserveSeats(List<Seat> reservedSeats)
        {
            try
            {
                var firstGroupSeat = reservedSeats.First();
                for (int i = 0; (i < _cinemaShow.MinDistance) && (firstGroupSeat.Column - i >= 0); i++)
                {
                    for (int j = 1; (j < _cinemaShow.MinDistance) && (firstGroupSeat.Row + j <= _cinemaShow.Rows); j++)
                    {
                        _cinemaShow.Seats[firstGroupSeat.Row + j - i, firstGroupSeat.Column - i].IsTaken = true;
                        if (firstGroupSeat.Row - j + i >= 0)
                        {
                            _cinemaShow.Seats[firstGroupSeat.Row - j + i, firstGroupSeat.Column - i].IsTaken = true;
                        }
                    }
                    _cinemaShow.Seats[firstGroupSeat.Row, firstGroupSeat.Column - i].IsTaken = true;
                }

                for (int i = 1; (i < reservedSeats.Count - 1 + _cinemaShow.MinDistance) && (firstGroupSeat.Column + i <= _cinemaShow.Columns); i++)
                {
                    for (int j = 1; (j < _cinemaShow.MinDistance) && (firstGroupSeat.Row + j <= _cinemaShow.Rows); j++)
                    {
                        if (i <= reservedSeats.Count - 1)
                        {
                            _cinemaShow.Seats[firstGroupSeat.Row + j, firstGroupSeat.Column + i].IsTaken = true;
                            if (firstGroupSeat.Row - j + i >= 0)
                            {
                                _cinemaShow.Seats[firstGroupSeat.Row - j, firstGroupSeat.Column + i].IsTaken = true;
                            }
                        }
                        _cinemaShow.Seats[firstGroupSeat.Row + j - i, firstGroupSeat.Column + i].IsTaken = true;
                        if (firstGroupSeat.Row - j + i >= 0)
                        {
                            _cinemaShow.Seats[firstGroupSeat.Row - j + i, firstGroupSeat.Column + i].IsTaken = true;
                        }
                    }
                    _cinemaShow.Seats[firstGroupSeat.Row, firstGroupSeat.Column + i].IsTaken = true;
                }
            }
            catch (Exception e)
            {
                return "RESERVE FAIL";
            }
            return "SUCCEED";
        }

        public List<AvailableSeatRow> PopulateOpenSeats(int seatCount)
        {
            List<AvailableSeatRow> seatsAvailable = new List<AvailableSeatRow>();
            //return row with all available columns
            var openSeats = from Seat item in _cinemaShow.Seats
                            where !item.IsTaken
                            group item.Column by item.Row into SameRowSeats
                            select new { Row = SameRowSeats.Key, Columns = SameRowSeats.ToList() };
            foreach (var row in openSeats)
            {
                //take only set that bigger than required seats
                var consecutiveSeatSets = AlgorithmExtend.GroupConsecutive(row.Columns).Where(l => l.Count() >= seatCount);
                foreach (var seatSet in consecutiveSeatSets)
                {
                    seatsAvailable.Add(new AvailableSeatRow { Row = row.Row, Columns = seatSet.ToList() });
                }
            }
            return seatsAvailable;
        }
    }
}
